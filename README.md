# Wedoogift-app

[![Build Status](https://travis-ci.org/codecentric/springboot-sample-app.svg?branch=master)](https://travis-ci.org/codecentric/springboot-sample-app)
[![Coverage Status](https://coveralls.io/repos/github/codecentric/springboot-sample-app/badge.svg?branch=master)](https://coveralls.io/github/codecentric/springboot-sample-app?branch=master)
[![License](http://img.shields.io/:license-apache-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)

Minimal [Spring Boot](http://projects.spring.io/spring-boot/), [Spring Batch](http://projects.spring.io/spring-batch/) Wedoogif app.

## Requirements

For building and running the application you need:

- [JDK 11](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 4](https://maven.apache.org)

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.challenge.wedoogift.WedoogiftApplication` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```

## Running tests locally
You should first comment CommandLineRunner bean in main WedoogiftApplication, and then
you can use the Spring Boot Maven plugin like so:

```shell
mvn clean install
```
## Entities
- Company
- User
- UserAccount
- Deposit
- enums : DepositStatus, DepositType

## Data base H2
- username: sa
- password: password

## How to test

- When  starting the application it initializes an H2 database with some data that we gonna use to test.
- Then an expired GIFT deposit with an amount already added, but with an available status which is wrong.
- The purpose is to use Spring Batch to change the deposit status based on its start date and end date avery 3 minutes.
- We have the possibility to add a new deposit using api : [distributeDeposit](http://localhost:8086/swagger-ui.html#!/company45controller/distributeDepositsUsingPOST)
- We can also calculate a user's balance using this api : [getUserAvailableBalance](http://localhost:8086/swagger-ui.html#!/user45controller/getUserAvailableBalanceUsingGET)

