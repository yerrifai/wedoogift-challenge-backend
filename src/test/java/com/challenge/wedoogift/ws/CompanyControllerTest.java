package com.challenge.wedoogift.ws;

import com.challenge.wedoogift.dtos.DistributeDTO;
import com.challenge.wedoogift.services.CompanyService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(CompanyController.class)
class CompanyControllerTest {

    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private CompanyService companyService;

    @Test
    void distributeDeposits() throws Exception {
        //given
       DistributeDTO distributeDTO = new DistributeDTO(
                1L,
                "484cc79c-2593-4a8c-baf7-64a832432a24",
                100,
                LocalDate.of(2020,1,1),
                LocalDate.of(2021,1,1),
               "GIFT"
       );

        mvc.perform(post("/api/company/distributeDeposit")
                        .content(objectMapper.writeValueAsString(distributeDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.companyId").exists())
                .andExpect(jsonPath("$.companyId").value(1L))
                .andExpect(jsonPath("$.accountId").exists())
                .andExpect(jsonPath("$.accountId").value("484cc79c-2593-4a8c-baf7-64a832432a24"))
                .andExpect(jsonPath("$.amount").exists())
                .andExpect(jsonPath("$.amount").value(100))
                .andExpect(jsonPath("$.depositType").exists())
                .andExpect(jsonPath("$.depositType").value("GIFT"));
    }
}