package com.challenge.wedoogift.services.impl;

import com.challenge.wedoogift.entities.Deposit;
import com.challenge.wedoogift.entities.User;
import com.challenge.wedoogift.entities.UserAccount;
import com.challenge.wedoogift.enums.DepositStatus;
import com.challenge.wedoogift.enums.DepositType;
import com.challenge.wedoogift.exceptions.AccountNotFoundException;
import com.challenge.wedoogift.repositories.DepositRepository;
import com.challenge.wedoogift.repositories.UserAccountRepository;
import com.challenge.wedoogift.services.UserService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImpTest {
    @Mock
    private  DepositRepository depositRepository;

    @Mock
    private  UserAccountRepository userAccountRepository;
    private UserServiceImp userService;

    @BeforeEach
    void setUp() {
        userService = new UserServiceImp(depositRepository, userAccountRepository);
    }

    @Test
    void canCalculateUserBalance() {
        //given
        String accountId = "account-id";
        UserAccount userAccount = new UserAccount(accountId, 0, new User(), List.of(new Deposit()));
        List<Deposit> deposits = List.of(
                new Deposit(null,
                        LocalDate.of(2022, 1, 1),
                        LocalDate.of(2023, 1, 1),
                        100,
                        DepositType.GIFT,
                        DepositStatus.AVAILABLE,
                        new UserAccount(accountId,100, null, null)),
                new Deposit(null,
                        LocalDate.of(2022, 1, 1),
                        LocalDate.of(2023, 2, 28),
                        100,
                        DepositType.MEAL,
                        DepositStatus.AVAILABLE,
                        new UserAccount(accountId,100, null, null))
        );

        //when
        when(userAccountRepository.findById(any())).thenReturn(Optional.of(userAccount));
        when(depositRepository.findByUserAccountIdAndStatus(any(), any())).thenReturn(deposits);

        userService.calculateUserBalance(accountId);

        //then
        ArgumentCaptor<String> accountIdArgumentCaptor = ArgumentCaptor.forClass(String.class);

            // verify that userAccountRepository was invoked with the right arg
        verify(userAccountRepository).findById(accountIdArgumentCaptor.capture());

            // verify that depositRepository was invoked with the right args
        verify(depositRepository).findByUserAccountIdAndStatus(accountIdArgumentCaptor.capture(), any());
        String capturedAccountId = accountIdArgumentCaptor.getValue();
        assertThat(capturedAccountId).isEqualTo(accountId);

        assertThat(userAccount.getBalance()).isEqualTo(200);
    }

    @Test
    void willThrowWhenAccountNotFound() {
        //given
        String accountId = "account-id";
        // then
        assertThatThrownBy(() -> userService.calculateUserBalance(accountId))
                .isInstanceOf(AccountNotFoundException.class)
                .hasMessageContaining("UserAccount not found");

        verify(userAccountRepository, never()).save(any());
    }
}