package com.challenge.wedoogift.services.impl;

import com.challenge.wedoogift.dtos.DistributeDTO;
import com.challenge.wedoogift.entities.Company;
import com.challenge.wedoogift.entities.Deposit;
import com.challenge.wedoogift.entities.User;
import com.challenge.wedoogift.entities.UserAccount;
import com.challenge.wedoogift.exceptions.BalanceNotSufficientException;
import com.challenge.wedoogift.repositories.CompanyRepository;
import com.challenge.wedoogift.repositories.DepositRepository;
import com.challenge.wedoogift.repositories.UserAccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CompanyServiceImplTest {

    @Mock
    private  UserAccountRepository userAccountRepository;
    @Mock
    private  DepositRepository depositRepository;
    @Mock
    private  CompanyRepository companyRepository;
    private CompanyServiceImpl companyService;

    @BeforeEach
    void setUp() {
        companyService = new CompanyServiceImpl(userAccountRepository, depositRepository, companyRepository);
    }

    @Test
    void canDistributeDeposits() {
        //given
        DistributeDTO distributeDTO = new DistributeDTO(1L, "account-id", 300, LocalDate.of(2022,1,1), LocalDate.of(2023,1,1),"GIFT");
        Company company = new Company(1L, "TESLA", 1000, List.of(new User()));
        UserAccount userAccount = new UserAccount(distributeDTO.getAccountId(), 0, new User(), List.of(new Deposit()));

        //when
        when(userAccountRepository.findById(any())).thenReturn(Optional.of(userAccount));
        when(companyRepository.findById(any())).thenReturn(Optional.of(company));

        companyService.distributeDeposits(distributeDTO);

        //then
        ArgumentCaptor<String> accountIdArgumentCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Long> companyIdArgumentCaptor = ArgumentCaptor.forClass(Long.class);

        // verify that userAccountRepository was invoked with the right arg
        verify(userAccountRepository).findById(accountIdArgumentCaptor.capture());

        // verify that companyRepository was invoked with the right arg
        verify(companyRepository).findById(companyIdArgumentCaptor.capture());

        String capturedAccountId = accountIdArgumentCaptor.getValue();
        assertThat(capturedAccountId).isEqualTo(distributeDTO.getAccountId());

        Long capturedCompanyId = companyIdArgumentCaptor.getValue();
        assertThat(capturedCompanyId).isEqualTo(distributeDTO.getCompanyId());

        assertThat(userAccount.getBalance()).isEqualTo(300);
        assertThat(company.getBalance()).isEqualTo(700);
    }

    @Test
    void willThrowWhenBalanceNotSufficient() {
        //given
        DistributeDTO distributeDTO = new DistributeDTO(
                1L,
                "account-id",
                1300,
                LocalDate.of(2022,1,1), LocalDate.of(2023,1,1),"GIFT");
        Company company = new Company(1L, "TESLA", 1000, List.of(new User()));
        UserAccount userAccount = new UserAccount(distributeDTO.getAccountId(), 0, new User(), List.of(new Deposit()));

        //when
        when(userAccountRepository.findById(any())).thenReturn(Optional.of(userAccount));
        when(companyRepository.findById(any())).thenReturn(Optional.of(company));
        // then
        assertThatThrownBy(() -> companyService.distributeDeposits(distributeDTO))
                .isInstanceOf(BalanceNotSufficientException.class)
                .hasMessageContaining("Balance not sufficient");

        verify(depositRepository, never()).save(any());
        verify(userAccountRepository, never()).save(any());
        verify(companyRepository, never()).save(any());
    }
}