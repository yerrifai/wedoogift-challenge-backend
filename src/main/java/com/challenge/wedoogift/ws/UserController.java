package com.challenge.wedoogift.ws;

import com.challenge.wedoogift.dtos.UserDTO;
import com.challenge.wedoogift.exceptions.AccountNotFoundException;
import com.challenge.wedoogift.services.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
@Slf4j
public class UserController {

    private final UserService userService;

    @GetMapping("/user/availableBalance")
    public ResponseEntity<UserDTO> getUserAvailableBalance(@RequestParam String userAccountId) throws AccountNotFoundException {
        log.info("Distribute deposit for company");
        return new ResponseEntity<>(userService.calculateUserBalance(userAccountId), HttpStatus.OK);
    }
}
