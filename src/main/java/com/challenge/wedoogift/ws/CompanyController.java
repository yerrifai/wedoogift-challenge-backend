package com.challenge.wedoogift.ws;

import com.challenge.wedoogift.dtos.DistributeDTO;
import com.challenge.wedoogift.services.CompanyService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
@Slf4j
public class CompanyController {
    private final CompanyService companyService;

    @PostMapping("/company/distributeDeposit")
    public ResponseEntity<DistributeDTO> distributeDeposits(@Valid @RequestBody DistributeDTO distributeDTO) {
        log.info("Distribute deposit for company");
        companyService
                .distributeDeposits(distributeDTO);
        return new ResponseEntity<>(distributeDTO, HttpStatus.OK);
    }
}
