package com.challenge.wedoogift.enums;

public enum DepositStatus {
    EXPIRED, AVAILABLE
}
