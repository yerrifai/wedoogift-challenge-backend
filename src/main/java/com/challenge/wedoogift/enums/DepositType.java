package com.challenge.wedoogift.enums;

public enum DepositType {
    GIFT, MEAL;
}
