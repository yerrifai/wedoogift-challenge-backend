package com.challenge.wedoogift.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAccount {
    @Id
    private String id;
    private double balance;
    @ManyToOne
    private User user;
    @OneToMany(mappedBy = "userAccount",fetch = FetchType.LAZY)
    private List<Deposit> accountDeposits;
}
