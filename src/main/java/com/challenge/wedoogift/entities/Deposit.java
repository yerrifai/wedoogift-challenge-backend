package com.challenge.wedoogift.entities;

import com.challenge.wedoogift.enums.DepositStatus;
import com.challenge.wedoogift.enums.DepositType;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "deposit")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Deposit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate startDate;
    private LocalDate endDate;
    private double amount;
    @Enumerated(EnumType.STRING)
    private DepositType type;
    @Enumerated(EnumType.STRING)
    private DepositStatus status;
    @ManyToOne
    private UserAccount userAccount;

}
