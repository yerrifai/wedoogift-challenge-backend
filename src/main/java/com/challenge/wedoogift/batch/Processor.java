package com.challenge.wedoogift.batch;

import com.challenge.wedoogift.entities.Deposit;
import com.challenge.wedoogift.enums.DepositStatus;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class Processor implements ItemProcessor<Deposit, Deposit> {
    @Override
    public Deposit process(Deposit deposit) throws Exception {
        // set status for deposits
        if(deposit.getEndDate().isBefore(LocalDate.now())) {
            deposit.setStatus(DepositStatus.EXPIRED);
        }
        return deposit;
    }
}
