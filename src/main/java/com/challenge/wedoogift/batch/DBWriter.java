package com.challenge.wedoogift.batch;

import com.challenge.wedoogift.entities.Deposit;
import com.challenge.wedoogift.repositories.DepositRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class DBWriter implements ItemWriter<Deposit> {

    private DepositRepository depositRepository;

    public DBWriter (DepositRepository depositRepository) {
        this.depositRepository = depositRepository;
    }

    @Override
    public void write(List<? extends Deposit> deposits) throws Exception {
        log.info("Data Saved for Deposits: " + deposits);
        depositRepository.saveAll(deposits);
    }
}
