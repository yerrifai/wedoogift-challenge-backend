package com.challenge.wedoogift.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = {StringValidator.class})
@Retention(RUNTIME)
@Target({FIELD, TYPE})
public @interface ValidateString {
    String[] acceptedValues();

    String message() default "Invalid dataType";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
