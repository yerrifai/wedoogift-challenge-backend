package com.challenge.wedoogift.config;

import com.challenge.wedoogift.entities.Deposit;
import com.challenge.wedoogift.entities.UserAccount;
import com.challenge.wedoogift.enums.DepositStatus;
import com.challenge.wedoogift.enums.DepositType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DepositRowMapper implements RowMapper<Deposit> {

    @Override
    public Deposit mapRow(ResultSet rs, int i) throws SQLException {
        Deposit deposit = new Deposit();
        deposit.setId(rs.getLong("id"));
        deposit.setStartDate(rs.getDate("start_Date").toLocalDate());
        deposit.setEndDate(rs.getDate("end_Date").toLocalDate());
        deposit.setAmount(rs.getDouble("amount"));
        deposit.setType(DepositType.valueOf(rs.getString("type")));
        deposit.setStatus(DepositStatus.valueOf(rs.getString("status")));
        UserAccount userAccount = new UserAccount();
        userAccount.setId(rs.getString("user_account_id"));
        deposit.setUserAccount(userAccount);
        return deposit;
    }
}
