package com.challenge.wedoogift.config;

import com.challenge.wedoogift.entities.Deposit;
import com.challenge.wedoogift.repositories.DepositRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing
public class SpringBatchConfig {

    private final DepositRepository depositRepository;
    private final DataSource dataSource;
    public SpringBatchConfig(DepositRepository depositRepository, DataSource dataSource) {
        this.depositRepository = depositRepository;
        this.dataSource = dataSource;
    }

    @Bean
    public Job job(JobBuilderFactory jobBuilderFactory,
                   StepBuilderFactory stepBuilderFactory,
                   ItemReader<Deposit> itemReader,
                   ItemProcessor<Deposit, Deposit> itemProcessor,
                   ItemWriter<Deposit> itemWriter) {

        Step step = stepBuilderFactory.get("ETL-deposit-status")
                .<Deposit, Deposit>chunk(100)
                .reader(itemReader)
                .processor(itemProcessor)
                .writer(itemWriter)
                .build();

        return jobBuilderFactory.get("ETL-Deposit")
                .incrementer(new RunIdIncrementer())
                .start(step)
                .build();
    }

    @Bean
    public JdbcCursorItemReader<Deposit> itemReader() {
        JdbcCursorItemReader<Deposit> cursorItemReader = new JdbcCursorItemReader<>();
        cursorItemReader.setDataSource(dataSource);
        cursorItemReader.setSql("SELECT id,amount,start_Date,end_Date,status,type,user_account_id FROM deposit");
        cursorItemReader.setRowMapper(new DepositRowMapper());
        return cursorItemReader;
    }
}
