package com.challenge.wedoogift;

import com.challenge.wedoogift.entities.Company;
import com.challenge.wedoogift.entities.Deposit;
import com.challenge.wedoogift.entities.User;
import com.challenge.wedoogift.entities.UserAccount;
import com.challenge.wedoogift.enums.DepositStatus;
import com.challenge.wedoogift.enums.DepositType;
import com.challenge.wedoogift.repositories.CompanyRepository;
import com.challenge.wedoogift.repositories.DepositRepository;
import com.challenge.wedoogift.repositories.UserAccountRepository;
import com.challenge.wedoogift.repositories.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.util.UUID;
import java.util.stream.Stream;

@SpringBootApplication()
public class WedoogiftApplication {
	public static void main(String[] args) {
		SpringApplication.run(WedoogiftApplication.class, args);
	}

	@Bean
	CommandLineRunner commandLineRunner(UserRepository userRepository,
										UserAccountRepository userAccountRepository,
										DepositRepository depositRepository,
										CompanyRepository companyRepository) {
		return args -> {

			Company company = new Company();
			company.setBalance(Math.random() * 90000);
			company.setName("TESLA");
			companyRepository.saveAndFlush(company);

			Stream.of("Mickael", "Philippe", "Youssef").forEach(name -> {
				User user = new User();
				user.setName(name);
				user.setEmail(name + "@gmail.com");
				//adding company
				user.setCompany(company);
				userRepository.save(user);
			});

			userRepository.findAll().forEach(user -> {
				UserAccount firstUserAccount = new UserAccount();
				firstUserAccount.setId(UUID.randomUUID().toString());
				firstUserAccount.setUser(user);
				userAccountRepository.save(firstUserAccount);

				UserAccount secondUserAccount = new UserAccount();
				secondUserAccount.setId(UUID.randomUUID().toString());
				secondUserAccount.setUser(user);
				userAccountRepository.save(secondUserAccount);
			});

			Deposit giftDeposit = new Deposit();
			giftDeposit.setType(DepositType.GIFT);
			giftDeposit.setStatus(DepositStatus.AVAILABLE);
			giftDeposit.setAmount(300L);
			if(userRepository.findById(1L).isPresent()) {
				User userById = userRepository.findById(1L).get();

				// get the first account of the user and set the balance
				UserAccount userAccount = userAccountRepository.findByUser(userById).get(0);
				userAccount.setBalance(userAccount.getBalance() + giftDeposit.getAmount());
				userAccountRepository.save(userAccount);

				giftDeposit.setUserAccount(userAccount);
			}
			giftDeposit.setStartDate(LocalDate.of(2020, 1,2));
			giftDeposit.setEndDate(LocalDate.of(2021, 1,2));
			depositRepository.save(giftDeposit);
		};
	}
}
