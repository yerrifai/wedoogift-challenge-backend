package com.challenge.wedoogift.dtos;

import com.challenge.wedoogift.validator.ValidateString;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;

@Data
@AllArgsConstructor
public class DistributeDTO {
    private Long companyId;
    private String accountId;
    private double amount;
    private LocalDate startDate;
    private LocalDate endDate;
    @NotEmpty
    @ValidateString(acceptedValues={"GIFT", "MEAL"}, message="Invalid depositType it accepts only GIFT or MEAL")
    private String depositType;
}
