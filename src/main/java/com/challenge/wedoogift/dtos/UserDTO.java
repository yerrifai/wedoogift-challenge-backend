package com.challenge.wedoogift.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserDTO {
    private String accountId;
    private double balance;
}
