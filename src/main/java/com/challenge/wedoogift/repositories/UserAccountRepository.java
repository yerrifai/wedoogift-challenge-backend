package com.challenge.wedoogift.repositories;

import com.challenge.wedoogift.entities.User;
import com.challenge.wedoogift.entities.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, String> {
    List<UserAccount> findByUser(User user);
}
