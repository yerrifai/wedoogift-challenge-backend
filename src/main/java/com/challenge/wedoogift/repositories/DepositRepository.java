package com.challenge.wedoogift.repositories;

import com.challenge.wedoogift.entities.Deposit;
import com.challenge.wedoogift.enums.DepositStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepositRepository extends JpaRepository<Deposit,Long> {
    List<Deposit> findByUserAccountIdAndStatus(String accountId, DepositStatus depositStatus);
}
