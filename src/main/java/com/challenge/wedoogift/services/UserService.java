package com.challenge.wedoogift.services;

import com.challenge.wedoogift.dtos.UserDTO;
import com.challenge.wedoogift.exceptions.AccountNotFoundException;

public interface UserService {
    UserDTO calculateUserBalance(String accountId) throws AccountNotFoundException;
}
