package com.challenge.wedoogift.services.impl;

import com.challenge.wedoogift.dtos.DistributeDTO;
import com.challenge.wedoogift.entities.Company;
import com.challenge.wedoogift.entities.Deposit;
import com.challenge.wedoogift.entities.UserAccount;
import com.challenge.wedoogift.enums.DepositStatus;
import com.challenge.wedoogift.enums.DepositType;
import com.challenge.wedoogift.exceptions.AccountNotFoundException;
import com.challenge.wedoogift.exceptions.BalanceNotSufficientException;
import com.challenge.wedoogift.repositories.CompanyRepository;
import com.challenge.wedoogift.repositories.DepositRepository;
import com.challenge.wedoogift.repositories.UserAccountRepository;
import com.challenge.wedoogift.services.CompanyService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.Month;

@Service
@Transactional
@AllArgsConstructor
@Slf4j
public class CompanyServiceImpl implements CompanyService {
    private final UserAccountRepository userAccountRepository;
    private final DepositRepository depositRepository;
    private final CompanyRepository companyRepository;

    /**
     * functions allowing companies to distribute deposits
     * @param distributeDTO distributeDTO
     * @throws AccountNotFoundException AccountNotFoundException
     * @throws BalanceNotSufficientException BalanceNotSufficientException
     */
    @Override
    public void distributeDeposits(DistributeDTO distributeDTO) throws AccountNotFoundException, BalanceNotSufficientException {
        log.info("Distributing new Deposit");

        // Check if company exists
        Company company = companyRepository.findById(distributeDTO.getCompanyId())
                .orElseThrow(()-> new AccountNotFoundException("Company not found"));

        // Check if userAccount exists
        UserAccount userAccount = userAccountRepository.findById(distributeDTO.getAccountId())
                .orElseThrow(()-> new AccountNotFoundException("UserAccount not found"));

        // Check if company balance is sufficient
        log.info("checking if balance is sufficient");
        if(company.getBalance() < distributeDTO.getAmount()) {
            throw new BalanceNotSufficientException("Balance not sufficient");
        }

        // balance is sufficient
        Deposit deposit = new Deposit();
        deposit.setType(distributeDTO.getDepositType().equalsIgnoreCase("gift") ? DepositType.GIFT : DepositType.MEAL);
        deposit.setAmount(distributeDTO.getAmount());

        //if start date and end date
        deposit.setStartDate(distributeDTO.getStartDate() != null ? distributeDTO.getStartDate() : LocalDate.now());
        deposit.setEndDate(distributeDTO.getDepositType().equalsIgnoreCase("gift") ? calculateDepositGiftEndDate(deposit.getStartDate()) : calculateDepositMealEndDate(deposit.getStartDate()));
        distributeDTO.setEndDate(deposit.getEndDate());
        distributeDTO.setStartDate(deposit.getStartDate());
        deposit.setUserAccount(userAccount);

        if(deposit.getEndDate().isBefore(LocalDate.now())) {
            deposit.setStatus(DepositStatus.EXPIRED);
        } else {
            deposit.setStatus(DepositStatus.AVAILABLE);
        }

        depositRepository.save(deposit);

        //update user balance
        userAccount.setBalance(userAccount.getBalance() + distributeDTO.getAmount());
        userAccountRepository.save(userAccount);

        //update company balance
        company.setBalance(company.getBalance() - distributeDTO.getAmount());
        companyRepository.save(company);
    }

    /**
     * calculate end date of gift deposit
     * @param startDate start date
     * @return endDate
     */
    private LocalDate calculateDepositGiftEndDate(LocalDate startDate) {
        return startDate.plusDays(365);
    }

    /**
     * calculate end date of Meal deposit
     * @param startDate start date
     * @return endDate
     */
    private LocalDate calculateDepositMealEndDate(LocalDate startDate) {
        LocalDate oneYearAfter = startDate.plusYears(1);
        return LocalDate.of(oneYearAfter.getYear(), Month.FEBRUARY, 28);
    }
}
