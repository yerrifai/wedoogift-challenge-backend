package com.challenge.wedoogift.services.impl;

import com.challenge.wedoogift.dtos.UserDTO;
import com.challenge.wedoogift.entities.Deposit;
import com.challenge.wedoogift.entities.UserAccount;
import com.challenge.wedoogift.enums.DepositStatus;
import com.challenge.wedoogift.exceptions.AccountNotFoundException;
import com.challenge.wedoogift.repositories.DepositRepository;
import com.challenge.wedoogift.repositories.UserAccountRepository;
import com.challenge.wedoogift.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@Slf4j
public class UserServiceImp implements UserService {
    private final DepositRepository depositRepository;
    private final UserAccountRepository userAccountRepository;

    public UserServiceImp(DepositRepository depositRepository, UserAccountRepository userAccountRepository) {
        this.depositRepository = depositRepository;
        this.userAccountRepository = userAccountRepository;
    }

    @Override
    public UserDTO calculateUserBalance(String accountId) throws AccountNotFoundException {
        log.info("start calculating user's balance: " + accountId);
        // Check if userAccount exists
        UserAccount userAccount = userAccountRepository.findById(accountId)
                .orElseThrow(()-> new AccountNotFoundException("UserAccount not found"));

        List<Deposit> depositsByStatusAndUserAccount = depositRepository
                .findByUserAccountIdAndStatus(accountId, DepositStatus.AVAILABLE);

        double sum = depositsByStatusAndUserAccount.stream().mapToDouble(Deposit::getAmount).sum();
        userAccount.setBalance(sum);
        userAccountRepository.save(userAccount);

        return new UserDTO(accountId, sum);
    }
}
