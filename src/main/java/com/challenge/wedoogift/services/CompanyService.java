package com.challenge.wedoogift.services;

import com.challenge.wedoogift.dtos.DistributeDTO;
import com.challenge.wedoogift.exceptions.AccountNotFoundException;
import com.challenge.wedoogift.exceptions.BalanceNotSufficientException;

public interface CompanyService {
    void distributeDeposits(DistributeDTO distributeDTO) throws AccountNotFoundException, BalanceNotSufficientException;
}
